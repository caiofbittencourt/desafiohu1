<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" type="text/css"  href="add/jquery-ui.min.css"/>
      <link href="font-awesome.min.css" rel="stylesheet">
    <title></title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/style.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <header>
    <nav class="navbar">
        <div class="container">
                <img src="images/logo.png" class="center-block img-responsive">
        </div>
    </nav>
    </header>
    <main>
      <section>
        <div class="section">
          <div class="container">
            <div class="row">
              <div class="col-md-12">
                <h1 class="text-center text"><strong>+ 170.000</strong> hotéis, pousadas e resorts no mundo todo.</h1>
              </div>
            </div>
          </div>
        </div>
        <form id="form" action="Daohu.php" method="post" class="form-inline">
        <div class="section content-buttons">
          <div class="container">
            <div class="row">
              <div class="col-md-4 col-md-offset-2">
                <h2><strong>Quer ficar onde?</strong></h2>
                <div class="input-group">
                  <span class="input-group-addon"><span class="fa fa-map-marker icon-button fa-2x" aria-hidden="true"></span></span>
                  <input type="text" id="localautocomplete" name="localautocomplete"  class="form-control btn-default btn-lg size-search" placeholder="cidade ou hotél" >
                </div>
              </div>
              <div class="col-md-6">
                <h2><strong>Quando?</strong>(Entrada e Saída)</h2>
                <div class="input-group size">
                  <span class="input-group-addon"><span class="fa fa-calendar icon-button fa-2x" aria-hidden="true"></span></span>
                  <input type="text" class="form-control btn-default btn-lg size-search" name="datae" placeholder="entrada" onfocus="(this.type='date')">
                </div>
                <div class="input-group size right">
                  <span class="input-group-addon"><span class="fa fa-calendar icon-button fa-2x" aria-hidden="true"></span></span>
                  <input type="text" class="form-control btn-default btn-lg size-search" name="datas" placeholder="saída" onfocus="(this.type='date')">
                </div>
                <div class="checkbox">
                  <label><input type="checkbox" value="">Ainda não defini as datas</label>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="section">
          <div class="container">
            <div class="row">
              <button id=buscar type="button" class="btn btn-default btn-lg center-block search-button">
                <span class="fa fa-search fa-2x" aria-hidden="true"></span> BUSCAR
              </button>
            </div>
          </div>
        </div>
      </form>
      </section>
    </main>
     <ul class="lista">
<?php
if (!empty($array_nome) && !empty($array_local)){
    $qts=count($array_nome);
for ($i=0; $i<$qts;$i++){
?>
  <li class="listaanuncio"><?php echo $array_local[$i] . ", ";
    echo $array_nome[$i]; ?></li>
            <?php
  }
}if (isset($array_nome)&& empty($array_nome))
    echo "Desculpe,não há hoteis disponíveis.";
?>
        </ul>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="add/jquery-3.1.1.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

    <script src="add/jquery-ui.min.js"></script>
  </body>
</html>
<script language="Javascript" text="text/javascript">

$(document).ready(function($){

  $('#buscar').click(function () {
    $('#form').submit();
  });

  $('#localautocomplete').autocomplete(
  {
      source:"sugestaolocal.php",
      minLength:2
  })
});
</script>